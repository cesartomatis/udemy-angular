import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowAddServers: boolean = false;
  addServerStr: string = 'No server added';
  serverName: string = 'Test Server';
  serverCreated: boolean = false;
  servers: Array<string> = ['Test Server 1', 'Test Server 2'];

  constructor() {
    setTimeout(() => {
      this.allowAddServers = true;
    },
      2000);
  }

  ngOnInit() {
  }
  onAddServerClicked() {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.addServerStr = `Added server with name ${this.serverName}`;
  }
  // onUpdateServerName(event: Event) {
  //   this.serverName = (<HTMLInputElement>event.target).value;
  // }
}
